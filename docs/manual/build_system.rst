..
 # Copyright (c) 2023, Linaro Ltd.
 #
 # SPDX-License-Identifier: MIT

############
Build System
############
Yocto with bitbake is the build engine used to build TRS. However, due to the
fact that a TRS builds various components such as firmware (Trusted Substrate),
root-filesystem, and kernel independently, we have wrapped the setup,
configuration, and build commands in a simple high-level Makefile. That way it
easy for people to get started with TRS by minimizing the number of commands
needed to get it working. Interested parties can also look at the inner
working of the Makefile to better understand how things come together.

Putting it in a Makefile makes it easy to additionally address any build
dependencies, i.e., we can control the build sequence, which may be desirable
and needed for an umbrella project such as TRS.

Target Platforms
****************
TRS supports multiple platforms, including Arm application profiles, execution
states, and extension types. However, TRS use the same OS and root filesystem
image for all devices. Therefore, there is currently not a need to describe the
platforms as such. Refer to the :ref:`Firmware - Trusted Substrate` section to
learn how to configure and build firmware for different target systems.

Distribution Image Features
***************************
TRS distro features can be found under ``./trs/meta-trs/conf/distro/trs.conf``.
In summary we append the following ``DISTRO_FEATURES`` in TRS.

.. program-output:: grep "^DISTRO_FEATURES:" ../../meta-trs/conf/distro/trs.conf | sort -u | sed 's/^.*= *"\(.*\)"$/\1/'
   :shell:


.. todo::
   We should show and explain all the distro features flags that are relevant to
   TRS. See this as an example:
   https://ewaol.docs.arm.com/en/kirkstone-dev/manual/build_system.html#distribution-image-features

..
 TODO: Seriously need help from a Yocto person to really explain and sort this
 out. This is what I came up with after grep'ing and reading various Yocto
 files. However, I barely understand what I've written myself.

As can be seen, we inherit and use EWAOL's ``ewaol-baremetal`` build for our
baremetal environment. Likewise for our **virtualization** build, we get Xen by
enabling ``ewaol-virtualization`` that can be found in
``./meta-ewaol/meta-ewaol-distro/conf/distro/ewaol.conf``
